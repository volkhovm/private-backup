#!/bin/sh
# This script produces a public backup of my secret info.

set -e

BACKUP_DIR=~/code/private-backup/
PRIVATE_DIR=~/reps/private/

cd $BACKUP_DIR

function update_sk() {
    set -e
    # To update secret key backup:
    # They are encrypted by default: https://stackoverflow.com/questions/9981099/are-exported-private-keys-in-gpg-still-encrypted
    gpg --export-options backup --export-secret-keys > seckey
    gpg --symmetric -o seckey.gpg --yes seckey
    rm seckey
}

function create_archive() {
    set -e
    echo "Checking the master passphrase:"
    gpg -d seckey.gpg > /dev/null

    gpg -a --export > ~/reps/private/gpg-pubkeys.asc

    pass git push

    cd $BACKUP_DIR
    tar czvf private.tgz ~/reps/private/
    gpg -e --yes -r 3B88C4272A0B49C8F0B0A78CF962509771F9CD1A private.tgz
    rm private.tgz
}

function pass_push() {
    pass git push
}

function github_push() {
    set -e
    git add .
    git commit -m "Bump"
    git push
}

function all() {
    set -e
    update_sk
    create_archive
    github_push
}

case "$1" in
    "update-sk")
        update_sk
        ;;
    "create-archive")
        create_archive
        ;;
    "push")
        pass_push
        github_push
        ;;
    "all")
        all;
        ;;
    *)
        echo "Wrong command. Choose on of the options:
    * update-sk
    * create-archive
    * push
    * all"
        exit
        ;;
esac
